**Installation**

In project composer file, put the following :

    "repositories": [{
    "type": "vcs",
    "url": "https://your_username@bitbucket.org/miniandmoreteam/component-notification.git"
    }],
    "require": {
      "mini-and-more/component-notification": "master"
    },
after that run 
    `composer update`
it will ask you for credentials enter your username and password and every thing will done.

**Usage**

After install package run this command:

`php artisan vendor:publish --tag=config`

in order to config your relation between `mail_logs` and `auth_table `.

in this line:

`'users_table'                   => 'users',`

after that run this:

`php artisan vendor:publish --provider=MiniAndMore\ComponentNotification\NotificationServiceProvider`

 
In config file we should declare our variables so this package will configured like this 

    'prefix_url' => 'notification-component',

    'slack' => [    

     'slack_webhook_url'=> 'https://hooks.slack.com/services/...',//https://hooks.slack.com/services/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
         
     'image'=> 'https://laravel.com/favicon.png',  
     
     'app_name' => ''],
     
     'middleware' => ['web','auth']

To use this package we need to specify  which user will be notified , from and the important attribute is with this will send notification or email or slack message our available ways are

    ['slack','mail','database']

Note that for Laravel version 5.7 or higher, you must install the notification channel via Composer in order for slack notifications to work properly:

     `composer require laravel/slack-notification-channel`

How to use it ex:

    $user=User::find(56709);
    
    $data=[
    'message'=>'This is automated message from '.config('app.name'),
    'user'=>$user,
    ];
    \ComponentNotification::from('memeaktaa@gmail.com')
    ->with(['mail','slack'])    
    ->subject('test')    
    ->markdown('mini-and-more::emails.mail')    
    ->send($user,$data);

We have interface to show emails_log and you have ability to resend, view Details, edit and resend email and finally you can view your email template.

if you don't want them to appear just set `show_views` parameter as `false` in `config` file:

    'show_views' => false
    
![alt text](./src/resources/assets/img/readme/actions.jpg) 
![alt text](./src/resources/assets/img/readme/view.jpg )
![alt text](./src/resources/assets/img/readme/edit-details.jpg )
![alt text](./src/resources/assets/img/readme/view-details.jpg )