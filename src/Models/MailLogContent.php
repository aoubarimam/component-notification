<?php

namespace MiniAndMore\ComponentNotification\Models;


class MailLogContent extends \Eloquent
{
	protected $fillable = ['mail_log_id', 'content'];
	protected $casts = [
		'content' => 'array',
	];

	public function getContentAttribute($content)
	{
		return collect(json_decode($content))->map(function ($item) {
			return collect($item);
		});
	}

	public function setContentAttribute($content)
	{
		$c = is_array($content)?$content:json_decode($content, true);
		$c['message'] = isset($c['message']) && is_array($c['message']) ? $c['message'][0] : $c['message']??'';
		$this->attributes['content'] = json_encode($c);
	}
}
