<?php

namespace MiniAndMore\ComponentNotification\Models;

class MailLog extends \Eloquent
{
    protected $fillable = [
        'email_template_id', 'subject', 'status', 'meta',
        'notify_to', 'notify_by', 'fired_id', 'fired_type', 'fired_at',
    ];
    protected $casts = [
        'meta' => 'array',
        'content' => 'array',
    ];

    public function content()
    {
        return $this->hasOne('MiniAndMore\ComponentNotification\Models\MailLogContent');
    }
}
