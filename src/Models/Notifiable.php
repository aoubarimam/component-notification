<?php

namespace MiniAndMore\ComponentNotification\Models;

class Notifiable extends \Eloquent
{
	use \Illuminate\Notifications\Notifiable;
	public $email;
	public $name;

	public function __construct($email, $name)
	{
		$this->email = $email;
		$this->name = $name;
	}

    public function routeNotificationForSlack()
    {
        return config('component-notification.slack.slack_webhook_url');
    }
}