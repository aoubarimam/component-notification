<?php

namespace MiniAndMore\ComponentNotification\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use MiniAndMore\ComponentNotification\Models\MailLog;
use MiniAndMore\ComponentNotification\Models\Notifiable;
use MiniAndMore\ComponentNotification\ComponentNotification;
use MiniAndMore\ComponentNotification\Notifications\DatabaseNotification;

class EmailsController extends Controller
{
	public function emailLogs()
	{
		$logs = MailLog::all();
		return view('component-notification::log', compact('logs'));
	}

	public function sendEmail(Request $request)
	{
		ComponentNotification::make()
			->from($request->get('from_email'), $request->get('from_name'))
			->cc($request->get('cc'))
			->bcc($request->get('bcc'))
			->with($request->get('with'))
			->send($request->get('content'));

		return response()->json(['msg' => __('Sent Successfully')]);
	}

	public function showEmail($mail_id)
	{
		$mail = MailLog::findOrFail($mail_id);
		return (new DatabaseNotification($mail['meta'] + ['content' => collect($mail->content->content)->toArray()]))->toMail();
	}

	public function viewDetailsEmail($mail_id)
	{
		$mail = MailLog::findOrFail($mail_id);

		return response()->json($mail['meta'] + ['content' => $mail->content->content]);
	}

	public function resendEmail($mail_id)
	{
		$mail = MailLog::findOrFail($mail_id);

		if ($mail['meta']['notifiable_type'] instanceof Notifiable) {
			$notifiable = new Notifiable($mail->notify_to, $mail->meta['from_name']);
		} else {
			$notifiable = $mail['meta']['notifiable_type']::where('email', $mail->notify_to)->first();
		}
		try {
			ComponentNotification::make()
				->from($mail['meta']['from'], $mail['meta']['from_name'])
				->cc($mail['meta']['cc'])
				->bcc($mail['meta']['bcc'])
				->with($mail['meta']['via'])
				->subject($mail['meta']['subject'])
				->send($notifiable, $mail->content->content->toArray(), $mail_id);

			$mail->update([
				'status' => 'success', 'fired_id' => \Auth::check() ? \Auth::id() : null,
				'fired_type' => \Auth::check() ? get_class(\Auth::user()) : null, 'fired_at' => now(),
			]);
		} catch (\Exception $exception) {
			return response()->json(['message' => $exception->getMessage()], 400);
		}

		return response()->json(['message' => __('Re Sent Successfully.')]);
	}

	public function editDetailsResendEmail($mail_id, Request $request)
	{
		$mail = MailLog::findOrFail($mail_id);
		if ($mail['meta']['notifiable_type'] == 'MiniAndMore\ComponentNotification\Models\Notifiable') {
			$notifiable = new Notifiable($mail->notify_to, $mail->meta['from_name']);
		} else {
			$notifiable = $mail['meta']['notifiable_type']::where('email', $mail->notify_to)->first();
		}
		$content = preg_replace('/[1-9]{1,2} items/', '', $request->get('content'));
		ComponentNotification::make()
			->from($request->get('from'), $request->get('from_name'))
			->cc($request->filled('cc') ? explode(',', $request->get('cc')) : null)
			->bcc($request->filled('bcc') ? explode(',', $request->get('bcc')) : null)
			->with($mail->meta['via'])
			->subject($request->get('subject'))
			->markdown($mail['meta']['markdown'])
			->send($notifiable, $content, $mail_id);

		$data = array_merge($mail['meta'], $request->except('_token') + [
				'via' => $mail->meta['via'], 'status' => 'success', 'fired_id' => \Auth::check() ? \Auth::id() : null,
				'fired_type' => \Auth::check() ? get_class(\Auth::user()) : null, 'fired_at' => now(),
			]);
		$mail->update($data);
		$mail->content()->update(['content' => $request->get('content')]);

		return redirect()->back()->withMsg(__('Sent Successfully'));
	}
}