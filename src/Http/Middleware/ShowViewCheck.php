<?php

namespace MiniAndMore\ComponentNotification\Http\Middleware;

use Closure;

class ShowViewCheck
{
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 *
	 * @return mixed
	 */
	public function handle($request, Closure $next)
	{
		if (!config('component-notification.show_views')) {
			return abort(404, __('Page Not Found'));
		}

		return $next($request);
	}
}
