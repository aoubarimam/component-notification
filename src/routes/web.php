<?php

Route::group([
    'prefix' => config('component-notification.prefix_url'),
    'namespace' => 'MiniAndMore\ComponentNotification\Http\Controllers',
    'middleware' => config('component-notification.middleware'),
], function () {
    \Route::get('/mail', 'EmailsController@emailLogs')->name('emails.index')->middleware(\MiniAndMore\ComponentNotification\Http\Middleware\ShowViewCheck::class);
    \Route::get('/mail/{mail_id}', 'EmailsController@showEmail')->name('email.show')->middleware(\MiniAndMore\ComponentNotification\Http\Middleware\ShowViewCheck::class);
    \Route::post('/mail', 'EmailsController@sendEmail');
    \Route::post('/mail-resend/{mail_id}', 'EmailsController@resendEmail')->name('email.resesnd');
    \Route::post('/mail-edit-details-resend/{mail_id}', 'EmailsController@editDetailsResendEmail')->name('email.edit-details-resend');
    \Route::get('/mail-view-details/{mail_id}', 'EmailsController@viewDetailsEmail')->name('email.view-details')->middleware(\MiniAndMore\ComponentNotification\Http\Middleware\ShowViewCheck::class);
});