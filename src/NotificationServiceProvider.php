<?php

namespace MiniAndMore\ComponentNotification;


use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;

class NotificationServiceProvider extends ServiceProvider
{

	public function boot(Router $router)
	{
		// register views
		$this->loadViewsFrom(__DIR__.'/resources/views', 'component-notification');

		// publish migrations
		$this->publishes([
			__DIR__.'/database/migrations/' => database_path('migrations'),
		], 'migrations');

		// publish config
		$this->publishes([
			__DIR__.'/Config/config.php' => config_path('component-notification.php'),
		], 'config');

		// publish styles
		$this->publishes([
			__DIR__.'/resources/assets' => public_path('vendor/miniandmore'),
		], 'public');

		// register routes
		$this->loadRoutesFrom(__DIR__.'/routes/web.php');

	}

	public function register()
	{
		\App::bind('componentnotification', ComponentNotification::class);
		//merge config files
		$config = __DIR__.'/Config/config.php';
		$this->mergeConfigFrom($config, 'component-notification');
	}

	protected function registerController()
	{
		// register our controller
		$this->app->make('MiniAndMore\ComponentNotification\Http\Controllers\EmailsController');
	}


}