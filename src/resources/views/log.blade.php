@extends('component-notification::layouts.app')

@section('title',__("Mail Log"))
@section('content')
    <div class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-md-12">
                    <div class="card">
                        <div class="card-header card-header-primary">
                            <label class="card-title ">Mail Log</label>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table datatable">
                                    <thead class=" text-primary">
                                    <th>#</th>
                                    <th>Subject</th>
                                    <th>To</th>
                                    <th>Status</th>
                                    <th>Via</th>
                                    </thead>
                                    <tbody>
                                    @foreach($logs as $log)
                                        <tr>
                                            <td class="text-primary">
                                                <div class="dropdown">
                                                    <a href="#pablo" id="navbarDropdownProfile" data-toggle="dropdown"
                                                       aria-haspopup="true" aria-expanded="false">
                                                        <i class="material-icons">grain</i>
                                                        <p class="d-lg-none d-md-block">
                                                            Actions
                                                        </p>
                                                    </a>
                                                    <div class="dropdown-menu dropdown-menu-right"
                                                         aria-labelledby="navbarDropdownProfile">
                                                        @if($log->status=='error')
                                                            <a class="dropdown-item resend"
                                                               href="{{route('email.resesnd',[$log->id])}}">Resend</a>
                                                        @endif

                                                        <a class="dropdown-item view-details"
                                                           href="{{route('email.view-details',[$log->id])}}">View
                                                            Details</a>

                                                        <a class="dropdown-item edit-details"
                                                           data-href="{{route('email.edit-details-resend',[$log->id])}}"
                                                           href="{{route('email.view-details',[$log->id])}}">Edit &
                                                            Resend</a>

                                                        <div class="dropdown-divider"></div>

                                                        <a class="dropdown-item view" target="_blank"
                                                           href="{{route('email.show',[$log->id])}}">View</a>
                                                    </div>
                                                </div>
                                            </td>
                                            <td>
                                                {{$log->subject}}
                                            </td>
                                            <td>
                                                {{$log->notify_to}}
                                            </td>
                                            <td class="{{$log->status=='success'?'text-success':'text-danger'}}">
                                                <span class="fa {{$log->status=='success'?'fa-check':'fa-close'}}"></span> {{ucfirst($log->status)}}
                                            </td>
                                            <td>
                                                @if(isset($log->meta))
                                                    @foreach($log->meta['via'] as $via)
                                                        <li>{{ucfirst($via)}}</li>
                                                    @endforeach
                                                @endif
                                            </td>
                                        </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="view-email">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <label class="modal-title">View Email</label>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="view-email-details">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">

                <!-- Modal Header -->
                <div class="modal-header">
                    <label class="modal-title">View Email</label>
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                </div>

                <!-- Modal body -->
                <div class="modal-body">
                    <div class="row">
                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>Subject: </b>
                                    <span class="ml-1" id="subject"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>From: </b>
                                    <span class="ml-1" id="from"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>From Name: </b>
                                    <span class="ml-1" id="from_name"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>To: </b>
                                    <span class="ml-1" id="notifiable_email"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>CC: </b>
                                    <span class="ml-1" id="cc"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-6">
                            <div class="form-group d-flex">
                                <label><b>BCC: </b>
                                    <span class="ml-1" id="bcc"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group d-flex">
                                <label><b>Message Content: </b>
                                    <div class="ml-1" >
                                        <p id="content"></p>
                                    </div>
                                </label>
                            </div>
                        </div>
                        <div class="col-lg-12">
                            <div class="form-group d-flex">
                                <label><b>Error Message: </b>
                                    <span class="ml-1" id="error_message"></span>
                                </label>
                            </div>
                        </div>
                    </div>
                </div>

                <!-- Modal footer -->
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>

                </div>

            </div>
        </div>
    </div>
    <div class="modal" id="edit-email-details">
        <div class="modal-dialog modal-lg">
            <form action="" class="edit-mail-form" method="post" enctype="multipart/form-data">
                @csrf
                <div class="modal-content">

                    <!-- Modal Header -->
                    <div class="modal-header">
                        <label class="modal-title">View Email</label>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>Subject: </b></label>
                                    <input type="text" id="subject" name="subject" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>From: </b></label>
                                    <input type="text" id="from" name="from" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>From Name: </b></label>
                                    <input type="text" id="from_name" name="from_name" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>To: </b></label>
                                    <input type="text" id="notifiable_email" disabled name="notifiable_email"
                                           class="form-control">
                                </div>
                            </div>

                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>CC: </b></label>
                                    <input type="text" id="cc" name="cc" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group d-flex">
                                    <label><b>BCC: </b></label>
                                    <input type="text" id="bcc" name="bcc" class="form-control">
                                </div>
                            </div>
                            <div class="col-lg-12">
                                <div class="form-group d-flex">
                                    <label ><b>Message Content: </b></label>
                                    <textarea id="content" rows="8" name="content" class="form-control d-none"></textarea>
                                    <pre id="json-display" class="mt-3"></pre>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="submit" class="btn btn-success">Save</button>
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>

                </div>
            </form>
        </div>
    </div>
@stop
@push('styles')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.3/css/bootstrap.css">
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/dataTables.bootstrap4.min.css">
    <style>
        #json-display{
            height: 300px;
            overflow-y: auto;
            width: 100%;
        }
    </style>
@endpush
@push('scripts')
    <script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.19/js/dataTables.bootstrap4.min.js"></script>
    <script src="vendor/miniandmore/js/jquery.json-editor.min.js"></script>
    <script>
        jQuery(function () {
            jQuery('.datatable').DataTable();
            jQuery('.datatable').on('click','.resend', function (e) {
                e.preventDefault();
                let $url = jQuery(this).attr('href');
                jQuery.ajax({
                    type: "POST",
                    url: $url,
                    success(xhr) {
                        md.showNotification('top', 'right', xhr.message, 'success');
                    },
                    error: HandleJsonErrors
                });
            });
            jQuery('.datatable').on('click','.view', function (e) {
                e.preventDefault();
                let $url = jQuery(this).attr('href');
                jQuery.ajax({
                    type: "GET",
                    url: $url,
                    success(xhr) {
                        jQuery('#view-email').modal('show')
                        jQuery('#view-email .modal-body').html(xhr)
                    },
                    error: HandleJsonErrors
                });
            });
            jQuery('.datatable').on('click','.view-details', function (e) {
                e.preventDefault();
                jQuery('#view-email-details').modal('show');
                $dialog = jQuery('#view-email-details .modal-body');
                let $url = jQuery(this).attr('href');
                jQuery('.load', $dialog).html('<div class="overlay"><i class="fa fa-spinner fa-2x fa-spin"></i></div>');
                jQuery.ajax({
                    type: "GET",
                    url: $url,
                    success(data) {
                        for (var i in data) {
                            if (typeof  data[i] === 'object') {
                                jQuery('#' + i, $dialog).html(JSON.stringify(data[i]));
                            }  else if ($.isArray(data[i])) {
                                jQuery('#' + i, $dialog).html(data[i].join(', '));
                            } else
                                jQuery('#' + i, $dialog).html(data[i]);
                        }
                        jQuery('.overlay', $dialog).remove();
                    },
                    error: HandleJsonErrors
                });
            });
            jQuery('.datatable').on('click','.edit-details', function (e) {
                e.preventDefault();
                jQuery('#edit-email-details').modal('show');
                jQuery('#edit-email-details form').attr('action', jQuery(this).attr('data-href'))
                $dialog = jQuery('#edit-email-details .modal-body');
                let $url = $(this).attr('href');
                jQuery('.load', $dialog).html('<div class="overlay"><i class="fa fa-spinner fa-2x fa-spin"></i></div>');
                jQuery.ajax({
                    type: "GET",
                    url: $url,
                    success(data) {
                        for (var i in data) {
                            if (typeof  data[i] === 'object') {
                                jQuery('#' + i, $dialog).html(JSON.stringify(data[i]));

                            } else if ($.isArray(data[i])) {
                                jQuery('#' + i, $dialog).val(data[i].join(', '));
                            } else
                                jQuery('#' + i, $dialog).val(data[i]);
                        }
                        var editor = new JsonEditor('#json-display', getJson());
                        jQuery('.overlay', $dialog).remove();
                    },
                    error: HandleJsonErrors
                });
            });
            jQuery('#json-display').on('keydown',function () {
                jQuery('[name=content]').val($(this).text());
            })
        });

        function getJson() {
            try {
                return JSON.parse(jQuery('[name=content]').val());
            } catch (ex) {
                alert('Wrong JSON Format: ' + ex);
            }
        }
    </script>
@endpush
