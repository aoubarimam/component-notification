<footer class="footer">
    <div class="container-fluid">
        <nav class="float-left">
        </nav>
        <div class="copyright float-right">
            &copy;
            <script>
                document.write(new Date().getFullYear())
            </script>
            <a href="{{url('/')}}" target="_blank">{{config('app.name')}}</a>.
        </div>
    </div>
</footer>