<div class="sidebar" data-color="rose" data-background-color="black" data-image="vendor/miniandmore/img/sidebar-1.jpg">
    <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
    <div class="logo">
        <a href="/" class="simple-text logo-normal">
            <img src="{{asset('images/logo.png')}}" alt="">
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">

            <li class="nav-item active ">
                <a class="nav-link" href="{{route('emails.index')}}">
                    <i class="material-icons">content_paste</i>
                    <p>Mail Managment</p>
                </a>
            </li>

        </ul>
    </div>
</div>