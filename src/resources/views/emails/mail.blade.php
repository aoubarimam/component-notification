@component('mail::layout')
    {{-- Header --}}
    @slot('header')
        @component('mail::header', ['url' => env('APP_URL')])
            {{config('app.name')}}
        @endcomponent
    @endslot

    {{-- Body --}}
    <div>
        <div>
            @if(isset($data))
                @if (!is_array($data))
                {!! $data !!}
                @else
                    <ul>
                        @foreach ($data as $datum)
                            <li>{!! $datum !!}</li>
                        @endforeach
                    </ul>
                @endif
            @endif

        </div>
        <p>
            Thank you for using our application,
            <br/>
            {{config('app.name')}} Team
        </p>
    </div>

    {{-- Footer --}}
    @slot('footer')
        @component('mail::footer')
            © {{ date('Y') }} Zoomaal. @lang('All rights reserved.')
        @endcomponent
    @endslot
@endcomponent