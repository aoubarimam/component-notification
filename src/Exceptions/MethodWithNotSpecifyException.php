<?php

namespace MiniAndMore\ComponentNotification\Exceptions;

use \RuntimeException;

class MethodWithNotSpecifyException extends RuntimeException
{
    protected $message = 'You should specify ways to send notification with: "with([])" method';
}