<?php

namespace MiniAndMore\ComponentNotification\Notifications;

use Illuminate\Notifications\Notification;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Messages\SlackMessage;

class DatabaseNotification extends Notification
{
    public $data;

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param mixed $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return $this->data['via'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @return array
     */
    public function toDatabase()
    {
        return [
            'message' => $this->data['content']['message'],
            'user_data' => isset($this->data['content']['user_data']) ? $this->data['content']['user_data'] : '',
        ];
    }

    /**
     * Get the array representation of the notification.
     *
     * @param mixed $notifiable
     * @return MailMessage
     */
    public function toMail()
    {
        return (new MailMessage())->from($this->data['from'], $this->data['from_name'])
            ->subject($this->data['subject'])
            ->cc($this->data['cc'] ?? [])
            ->bcc($this->data['bcc'] ?? [])
            ->replyTo($this->data['replyTo'] ?? [])
            ->markdown($this->data['markdown'], $this->data['content']);
    }

    public function toSlack()
    {
        return (new SlackMessage())
            ->from(config('component-notification.slack.app_name', $this->data['from_name']))
            //this if i want to specify channel or we just use routeNotificationForSlack for each user
            ->to(config('component-notification.slack.channel'))
            ->image(config('component-notification.slack.image'))
            ->content($this->data['content']['content']);
    }

    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
