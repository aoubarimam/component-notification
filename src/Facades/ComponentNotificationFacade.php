<?php

namespace MiniAndMore\ComponentNotification\Facades;

use Illuminate\Support\Facades\Facade;

class ComponentNotificationFacade extends Facade
{
    protected static function getFacadeAccessor()
    {
        return 'componentnotification';
    }
}