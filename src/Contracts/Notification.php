<?php


namespace MiniAndMore\ComponentNotification\Contracts;


/**
 * Interface Notification
 * @package MiniAndMore\ComponentNotification\Contracts
 */
interface Notification
{
    public function cc($cc = []);

    public function bcc($bcc = []);

    public function from($email, $name);

    public function subject($subject);

    public function markdown($markdown);

    public function with($sendMethods = []);

    public function replyTo($addresses = []);

    public function send($notifiable, $content, $mail_id);

	public function priority($level);

}