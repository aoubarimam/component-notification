<?php

namespace MiniAndMore\ComponentNotification;

use Illuminate\Container\Container;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Mail\Markdown;
use MiniAndMore\ComponentNotification\Contracts\Notification;
use MiniAndMore\ComponentNotification\Exceptions\MethodWithNotSpecifyException;
use MiniAndMore\ComponentNotification\Models\MailLog;
use MiniAndMore\ComponentNotification\Models\MailLogContent;
use MiniAndMore\ComponentNotification\Models\Notifiable;
use MiniAndMore\ComponentNotification\Notifications\DatabaseNotification;

/**
 * Class NotificationComp
 * @package MiniAndMore\ComponentNotification
 */
class ComponentNotification implements Notification
{
    /**
     * @var $from
     */
    protected $from;

    /**
     * @var $from_name
     */
    protected $from_name;

    /**
     * @var $to
     */

    protected $to;
    /**
     * @var $to_name
     */
    protected $to_name;

    /**
     * @var $subject
     */
    protected $subject;

    /**
     * @var $cc
     */
    protected $cc = [];

    /**
     * @var $bcc
     */
    protected $bcc = [];

    /**
     * @var $with
     */
    protected $with = [];

    /**
     * @var $with
     */
    protected $replyTo = [];

    /**
     * @var $markdown
     */

    protected $markdown = '';

    /**
     * @var $notifible
     */
    protected $notifible;

    /**
     * @var $priority
     */
    protected $priority;
    protected $mail_service;
    protected $sendgrid_api_key;

    public function __construct()
    {
        //miniandmore
        $this->from = config('component-notification.email_from');
        $this->from_name = config('component-notification.name_from');
        $this->subject = config('component-notification.subject');
        $this->mail_service = config('component-notification.mail_service');
        $this->sendgrid_api_key = config('component-notification.sendgrid_api_key');
    }

    public static function make()
    {
        return new self();
    }

    public static function url()
    {
        return url(config('component-notification.prefix_url') . '/mail');
    }

    /**
     * @param \Illuminate\Support\Collection|array|mixed $notifiable
     * @param null $content
     * @param null $mail_id
     */
    public function send($notifiable, $content = null, $mail_id = null)
    {
        if (empty($this->with)) {
            throw new MethodWithNotSpecifyException();
        }

        $content = is_array($content) ? $content : json_decode($content, true);

        if (!is_array($notifiable)) {
            $notifiable = array($notifiable);
        }
        foreach ($notifiable as $item) {
            if (!$item instanceof Model) {
                $item = new Notifiable($item, $this->from_name);
            }
            $params = [
                'from' => $this->from,
                'from_name' => $this->from_name,
                'mail_service' => $this->mail_service,
                'notifiable_email' => $item->email,
                'notifiable_type' => get_class($item),
                'subject' => $this->subject,
                'markdown' => $this->markdown,
                'cc' => $this->cc,
                'bcc' => $this->bcc,
                'via' => $this->with,
                'replyTo' => $this->replyTo,
            ];
            try {
                switch ($this->mail_service) {
                    case 'sendgrid':
                        $this->useSendGridService($item->email,
                            $params + ['content' => $content]);
                        break;
                    default:
                        \Notification::send($item, new DatabaseNotification(
                            $params + ['content' => $content]
                        ));
                        break;
                }
                $status = "success";
            } catch (\Exception $exception) {
                $params['error_message'] = $exception->getMessage();
                $status = "error";
            }

            if (in_array('mail', $this->with)) {
                $maillog = [
                    'subject' => $this->subject,
                    'status' => $status,
                    'meta' => $params,
                    'notify_to' => $item->email,
                    'notify_by' => $this->from,
                    'fired_id' => \Auth::check() ? \Auth::id() : null,
                    'fired_type' => \Auth::check() ? get_class(\Auth::user()) : null,
                    'fired_at' => now(),
                ];

                if (isset($content['email_template_id']) && $content['email_template_id'] > 0) {
                    $maillog['email_template_id'] = $content['email_template_id'];
                }

                $mail = MailLog::updateOrCreate(['id' => $mail_id], $maillog);
                MailLogContent::updateOrCreate(
                    ['mail_log_id' => $mail->id],
                    ['content' => $content]);
            }
        }
    }

    /**
     * @param $email
     * @param string $name
     * @return $this
     */
    public function from($email, $name = '')
    {
        $this->from = $email;
        $this->from_name = $name;
        return $this;
    }

    /**
     * @param $subject
     * @return $this
     */
    public function subject($subject)
    {
        $this->subject = $subject;
        return $this;
    }

    /**
     * @param $markdown
     * @return $this
     */
    public function markdown($markdown)
    {
        $this->markdown = $markdown;
        return $this;
    }

    /**
     * @param array $cc
     * @return $this
     */
    public function cc($cc = [])
    {
        $this->cc = $cc;
        return $this;
    }

    /**
     * @param array $bcc
     * @return $this
     */
    public function bcc($bcc = [])
    {
        $this->bcc = $bcc;
        return $this;
    }

    /**
     * @param array $with
     * @return $this
     */
    public function with($with = [])
    {
        $this->with = $with;
        return $this;
    }

    /**
     * @param array|string $replyTo
     * @return $this
     */
    public function replyTo($replyTo = [])
    {
        $this->replyTo = $replyTo;
        return $this;
    }

    /**
     * @param int $level
     * @return $this
     */
    public function priority($level)
    {
        $this->priority = $level;

        return $this;
    }

    private function useSendGridService(string $email, array $data)
    {
        if (!$this->sendgrid_api_key) {
            throw new \Exception('SendGrid api key should not be empty!');
        }
        $email_grid = new \SendGrid\Mail\Mail();
        $email_grid->setFrom($data['from'], $data['from_name']);
        $email_grid->setSubject($data['subject']);
        $email_grid->addTo($email);
        $email_grid->addContent(
            "text/html",
            (string) Container::getInstance()
                ->make(Markdown::class)
                ->render($data['markdown'], $data['content'])
        );
        $sendgrid = new \SendGrid($this->sendgrid_api_key);
        $response = $sendgrid->send($email_grid);
        if ($response->statusCode() != 202) {
            throw new \Exception($response->body());
        }
    }
}
